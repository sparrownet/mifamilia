package com.davidi.mifamilia;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.davidi.mifamilia.db.Person;

import java.util.List;

/**
 * Created by Dror on 25-Jun-16.
 */
public class RecyclerViewMinimizedAdapter extends RecyclerView.Adapter<RecyclerViewMinimizedAdapter.PersonViewHolder> {

    List<Person> mPersons;

    public RecyclerViewMinimizedAdapter(List<Person> locations){
        this.mPersons = locations;
    }

    @Override
    public int getItemCount() {
        return mPersons.size();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_person_minimized, viewGroup, false);
        PersonViewHolder lvh = new PersonViewHolder(v);
        return lvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.personName.setText(mPersons.get(i).getFirstName() + " " + mPersons.get(i).getLastName());
        personViewHolder.personId.setText("ID: " + mPersons.get(i).getId());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class PersonViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView personName;
        TextView personId;

        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.card_view);
            personName = (TextView)itemView.findViewById(R.id.person_name);
            personId = (TextView)itemView.findViewById(R.id.person_id);
        }
    }
}