package com.davidi.mifamilia;

import android.content.Context;
import android.text.TextUtils;

import com.davidi.mifamilia.api.FamilyApi;
import com.davidi.mifamilia.db.Person;
import com.davidi.mifamilia.db.Relationship;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dror on 01-Jul-16.
 */
public class MiFamiliaApi extends FamilyApi {

    public MiFamiliaApi(Context context) {
        super(context);
    }

    public long addPerson(Person person) {
        return super.addPerson(person);
    }

    /**
     * add a baby to the database, and update database accordingly
     * @param baby
     * @param parentA
     * @param parentB
     */
    public void addNewBaby(Person baby, Person parentA, Person parentB) {

        ArrayList<Person> parents = new ArrayList<>();
        parents.add(parentA);
        parents.add(parentB);

        // add the baby
        addPerson(baby);

        // update parents relationship
        for (Person parent : parents) {
            createRelationship(new Relationship(parent.getId(), baby.getId(), RelationshipType.CHILD.getValue()));
            if (TextUtils.equals(parent.getGender(), GENDER_MALE)) {
                createRelationship(new Relationship(baby.getId(), parent.getId(), RelationshipType.FATHER.getValue()));
            } else if (TextUtils.equals(parent.getGender(), GENDER_FEMALE)) {
                createRelationship(new Relationship(baby.getId(), parent.getId(), RelationshipType.MOTHER.getValue()));
            }

            // update siblings data
            List<Person> childrenList = getChildrenOf(parent);
            for (Person child : childrenList) {
                if (!TextUtils.equals(child.getId(), baby.getId())) {
                    createRelationship(new Relationship(baby.getId(), child.getId(), RelationshipType.SIBLING.getValue()));
                    createRelationship(new Relationship(child.getId(), baby.getId(), RelationshipType.SIBLING.getValue()));
                }
            }
        }
    }

    public void death(Person person) {
        deletePerson(person);
    }

    /**
     * update marriage relationship between couples
     * @param personA
     * @param personB
     */
    public void updateMarriage(Person personA, Person personB) {
        createRelationship(new Relationship(personA.getId(), personB.getId(), RelationshipType.MARRIED.getValue()));
        createRelationship(new Relationship(personB.getId(), personA.getId(), RelationshipType.MARRIED.getValue()));
    }
}
