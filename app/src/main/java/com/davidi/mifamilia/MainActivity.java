package com.davidi.mifamilia;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.davidi.mifamilia.db.Person;

/**
 * Created by Dror on 29-Jun-16.
 */
public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // add buttons and click listeners
        LinearLayout mShowPersonsBtn = (LinearLayout) findViewById(R.id.all_people_button);
        if (mShowPersonsBtn != null) {
            mShowPersonsBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), PersonsActivity.class);
                    startActivity(intent);
                }
            });
        }

        LinearLayout mAddPersonBtn = (LinearLayout) findViewById(R.id.add_person_button);
        if (mAddPersonBtn != null) {
            mAddPersonBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), AddPersonActivity.class);
                    startActivity(intent);
                }
            });
        }

        LinearLayout mSetMarriageBtn = (LinearLayout) findViewById(R.id.set_marriage_button);
        if (mSetMarriageBtn != null) {
            mSetMarriageBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), MarriageActivity.class);
                    startActivity(intent);
                }
            });
        }

        LinearLayout mAddBabyBtn = (LinearLayout) findViewById(R.id.add_baby_button);
        if (mAddBabyBtn != null) {
            mAddBabyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), AddPersonActivity.class);
                    intent.putExtra(AddPersonActivity.EXTRA_IS_BABY, true);
                    startActivity(intent);
                }
            });
        }

        TextView mAboutBtn = (TextView) findViewById(R.id.about_btn);
        if (mAboutBtn != null) {
            mAboutBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), AboutActivity.class);
                    startActivity(intent);
                }
            });
        }
    }
}
