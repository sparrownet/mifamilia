package com.davidi.mifamilia;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.davidi.mifamilia.db.Person;

import java.util.List;

/**
 * Created by Dror on 25-Jun-16.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.PersonViewHolder> {

    List<Person> mPersons;

    public RecyclerViewAdapter(List<Person> locations){
        this.mPersons = locations;
    }

    @Override
    public int getItemCount() {
        return mPersons.size();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_person, viewGroup, false);
        PersonViewHolder lvh = new PersonViewHolder(v);
        return lvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.personName.setText(mPersons.get(i).getFirstName() + " " + mPersons.get(i).getLastName());
        personViewHolder.personId.setText("ID: " + mPersons.get(i).getId());
        personViewHolder.personGender.setText("Gender: " + mPersons.get(i).getGender());
        personViewHolder.personBirthYear.setText("Birth Year: " + mPersons.get(i).getBirthYear());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class PersonViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView personName;
        TextView personId;
        TextView personGender;
        TextView personBirthYear;

        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.card_view);
            personName = (TextView)itemView.findViewById(R.id.person_name);
            personId = (TextView)itemView.findViewById(R.id.person_id);
            personGender = (TextView)itemView.findViewById(R.id.person_gender);
            personBirthYear = (TextView)itemView.findViewById(R.id.person_birth_year);
        }
    }
}