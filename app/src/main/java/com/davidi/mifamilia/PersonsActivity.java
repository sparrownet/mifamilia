package com.davidi.mifamilia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.davidi.mifamilia.db.Person;

import java.util.List;

/**
 * Created by Dror on 01-Jul-16.
 */
public class PersonsActivity extends AppCompatActivity {

    private List<Person> mPersonsList;
    private RecyclerViewAdapter mAdapter;
    private RecyclerView mRecyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_persons);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        // set recycler view
        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(llm);

        mRecyclerView.addOnItemTouchListener(
            new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                @Override public void onItemClick(View view, int position) {
                    // TODO Handle item click
                    String id = mPersonsList.get(position).getId();
                    Intent intent = new Intent(PersonsActivity.this, IndividualActivity.class);
                    intent.putExtra(IndividualActivity.EXTRA_ID, id);
                    startActivity(intent);
                }
            })
        );

        initializeData();
    }

    private void initializeData(){

        MiFamiliaApi api = new MiFamiliaApi(getApplicationContext());
        mPersonsList = api.getAllPersons();
        if (mPersonsList != null && mPersonsList.size() > 0) {
            mAdapter = new RecyclerViewAdapter(mPersonsList);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            Toast.makeText(this, R.string.error_no_persons_in_db, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.general_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_home:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
