package com.davidi.mifamilia;

import android.content.Context;

import com.davidi.mifamilia.db.Person;

/**
 * Created by Dror on 01-Jul-16.
 * This class fills the database with demo data for application testing
 *
 *
 * This is the demo families tree:
 *
 *
 *
 *      {ALON<---->RIVKA}               {SHAUL<---->JACKIE}
 *              |                               |
 *         LIHI , {YOSSI<--------------->XENA} , YONI          {ALEXANDER<---->ISRAELA}
 *                              |                                          |
 *                    MOSHE , JOHN , {SHIMON<--------------------------->LUCI}          {BARAK<---->SHULA}
 *                                                          |                                    |
 *                                                  JACK , RON , {SIMA<------------------->DROR} , KIRBY
 *                                                                             |
 *                                                                           MICHAEL
 */
public class DatabaseFiller {

    private MiFamiliaApi mApi;

    public DatabaseFiller(Context context) {
        mApi = new MiFamiliaApi(context);
    }

    public void fillData() {
        // DEMO adding data
        mApi.addPerson(new Person("0001", "Alon", "Go", "Male", "1890"));
        mApi.addPerson(new Person("0015", "Rivka", "Rik", "Female", "1915"));
        mApi.updateMarriage(mApi.getPerson("0001"), (mApi.getPerson("0015")));
        mApi.addNewBaby(new Person("0003", "Yossi", "Lock", "Male","1930"),
                mApi.getPerson("0001"),
                mApi.getPerson("0015"));
        mApi.addNewBaby(new Person("1005", "Lihi", "Cohen", "Female", "1932"),
                mApi.getPerson("0001"),
                mApi.getPerson("0015"));


        mApi.addPerson(new Person("0101", "Shaul", "Shosh", "Male","1906"));
        mApi.addPerson(new Person("0013", "Jackie", "Mitch", "Female", "1907"));
        mApi.updateMarriage(mApi.getPerson("0101"), (mApi.getPerson("0013")));
        mApi.addNewBaby(new Person("0016", "Yoni", "Bell", "Male","1927"),
                mApi.getPerson("0101"),
                mApi.getPerson("0013"));
        mApi.addNewBaby(new Person("0020", "Xena", "Right", "Female", "1933"),
                mApi.getPerson("0101"),
                mApi.getPerson("0013"));


        mApi.updateMarriage(mApi.getPerson("0020"), (mApi.getPerson("0003")));
        mApi.addNewBaby(new Person("0012", "Shimon", "Chan", "Male","1950"),
                mApi.getPerson("0020"),
                mApi.getPerson("0003"));
        mApi.addNewBaby(new Person("0103", "Moshe", "Cohen", "Male","1954"),
                mApi.getPerson("0020"),
                mApi.getPerson("0003"));
        mApi.addNewBaby(new Person("0888", "John", "Snow", "Male","1954"),
                mApi.getPerson("0020"),
                mApi.getPerson("0003"));

        mApi.addPerson(new Person("0115", "Alexander", "Bell", "Male","1935"));
        mApi.addPerson(new Person("0022", "Israela", "Chan", "Female", "1940"));
        mApi.updateMarriage(mApi.getPerson("0115"), (mApi.getPerson("0022")));
        mApi.addNewBaby(new Person("0009", "Luci", "Lue", "Female", "1961"),
                mApi.getPerson("0115"),
                mApi.getPerson("0022"));

        mApi.updateMarriage(mApi.getPerson("0009"), (mApi.getPerson("0012")));
        mApi.addNewBaby(new Person("0223", "Jack", "Bell", "Male","1977"),
                mApi.getPerson("0009"),
                mApi.getPerson("0012"));
        mApi.addNewBaby(new Person("0006", "Ron", "Ron", "Male","1980"),
                mApi.getPerson("0009"),
                mApi.getPerson("0012"));
        mApi.addNewBaby(new Person("0090", "Sima", "Davidi", "Female", "1982"),
                mApi.getPerson("0009"),
                mApi.getPerson("0012"));

        mApi.addPerson(new Person("0311", "Barak", "Davidi", "Male","1958"));
        mApi.addPerson(new Person("0021", "Shula", "Fleen", "Female", "1960"));
        mApi.updateMarriage(mApi.getPerson("0311"), (mApi.getPerson("0021")));
        mApi.addNewBaby(new Person("0007", "Dror", "Davidi", "Male","1983"),
                mApi.getPerson("0311"),
                mApi.getPerson("0021"));
        mApi.addNewBaby(new Person("0010", "Kirby", "Good", "Male","1995"),
                mApi.getPerson("0311"),
                mApi.getPerson("0021"));

        mApi.updateMarriage(mApi.getPerson("0007"), (mApi.getPerson("0090")));
        mApi.addNewBaby(new Person("0008", "Michael", "Davidi", "Male","2015"),
                mApi.getPerson("0007"),
                mApi.getPerson("0090"));

    }
}
