package com.davidi.mifamilia;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.davidi.mifamilia.db.Person;

import java.util.List;

/**
 * Created by Dror on 02-Jul-16.
 */
public class MarriageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marriage);

        final MiFamiliaApi api = new MiFamiliaApi(getApplicationContext());
        List<Person> personsList = api.getAllPersons();

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(R.string.set_marriage_title);

        // set spinners and adapters
        final Spinner spinnerPersonA = (Spinner) findViewById(R.id.person_a_spinner);
        ArrayAdapter<Person> adapterA = new ArrayAdapter<>(this, R.layout.spinner_item, personsList);
        adapterA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPersonA.setAdapter(adapterA);

        final Spinner spinnerPersonB = (Spinner) findViewById(R.id.person_b_spinner);
        ArrayAdapter<Person> adapterB = new ArrayAdapter<>(this,R.layout.spinner_item, personsList);
        adapterB.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPersonB.setAdapter(adapterB);

        // set the Save button
        Button btnSave = (Button) findViewById(R.id.btn_save_marriage);
        if (btnSave != null) {
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Person personA = (Person)spinnerPersonA.getSelectedItem();
                    Person personB = (Person)spinnerPersonB.getSelectedItem();

                    if (!TextUtils.equals(personA.getId(),personB.getId())) {
                        api.updateMarriage(personA, personB);
                        Toast.makeText(getApplicationContext(), "updated marriage of: " +
                                personA + " and " + personB, Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.error_invalid_selection, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
