package com.davidi.mifamilia;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.davidi.mifamilia.db.Person;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Dror on 02-Jul-16.
 */
public class AddPersonActivity extends AppCompatActivity {

    public static final String EXTRA_IS_BABY = "isBaby";

    private boolean mIsBaby;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person);

        final MiFamiliaApi api = new MiFamiliaApi(getApplicationContext());
        List<Person> personsList = api.getAllPersons();

        Intent incomingIntent = getIntent();
        mIsBaby = incomingIntent.getBooleanExtra(EXTRA_IS_BABY, false);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (getSupportActionBar() != null) {
            if (!mIsBaby) {
                getSupportActionBar().setTitle(R.string.add_person_title);
            } else {
                getSupportActionBar().setTitle(R.string.add_baby_title);
            }
        }

        // gender spinner
        final Spinner spinner = (Spinner) findViewById(R.id.gender_spinner);
        List<String> gendersList = Arrays.asList(MiFamiliaApi.GENDER_MALE, MiFamiliaApi.GENDER_FEMALE);
        ArrayAdapter<String> adapterGender = new ArrayAdapter<>(this, R.layout.spinner_item, gendersList);
        adapterGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if (spinner != null) {
            spinner.setAdapter(adapterGender);
        }

        // parentA spinner
        LinearLayout spinnerParentALayout = (LinearLayout) findViewById(R.id.person_a_spinner_container);
        final Spinner spinnerPersonA = (Spinner) findViewById(R.id.person_a_spinner);
        ArrayAdapter<Person> adapterA = new ArrayAdapter<>(this, R.layout.spinner_item, personsList);
        adapterA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if (spinnerPersonA != null) {
            spinnerPersonA.setAdapter(adapterA);
        }

        // parentB spinner
        LinearLayout spinnerParentBLayout = (LinearLayout) findViewById(R.id.person_b_spinner_container);
        final Spinner spinnerPersonB = (Spinner) findViewById(R.id.person_b_spinner);
        ArrayAdapter<Person> adapterB = new ArrayAdapter<>(this,R.layout.spinner_item, personsList);
        adapterB.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if (spinnerPersonB != null) {
            spinnerPersonB.setAdapter(adapterB);
        }

        if (mIsBaby) {
            spinnerParentALayout.setVisibility(View.VISIBLE);
            spinnerParentBLayout.setVisibility(View.VISIBLE);
        }

        Button saveBtn = (Button) findViewById(R.id.btn_save);
        if (saveBtn != null) {
            saveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText idEditText = (EditText) findViewById(R.id.input_id) ;
                    EditText firstNameEditText = (EditText) findViewById(R.id.input_first_name);
                    EditText lastNameEditText = (EditText) findViewById(R.id.input_last_name);
                    EditText birthYearEditText = (EditText) findViewById(R.id.input_birth);

                    // TODO in this demo we do not check input validity, so I wrap in try..catch
                    MiFamiliaApi api = new MiFamiliaApi(getApplicationContext());

                    String id = idEditText.getText().toString();
                    String firstName = firstNameEditText.getText().toString();
                    String lastName = lastNameEditText.getText().toString();
                    String gender = spinner.getSelectedItem().toString();
                    String year = birthYearEditText.getText().toString();

                    Person parentA = (Person) spinnerPersonA.getSelectedItem();
                    Person parentB = (Person) spinnerPersonB.getSelectedItem();

                    if (mIsBaby && TextUtils.equals(parentA.getId(),parentB.getId())) {
                        Toast.makeText(getApplicationContext(), R.string.error_require_different_parents,
                                Toast.LENGTH_SHORT).show();
                    } else {
                        if (!TextUtils.isEmpty(id) &&
                                !TextUtils.isEmpty(firstName) &&
                                !TextUtils.isEmpty(lastName) &&
                                !TextUtils.isEmpty(gender) &&
                                !TextUtils.isEmpty(year)) {

                            Person newPerson = new Person(id, firstName, lastName, gender, year);
                            long result = api.addPerson(newPerson);
                            if (result == -1) {
                                Toast.makeText(getApplicationContext(), "cannot add user, ID is not unique",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                // if baby
                                if (mIsBaby) {
                                    api.addNewBaby(newPerson, parentA, parentB);
                                }

                                String name = firstName + " " + lastName;
                                Toast.makeText(getApplicationContext(), name + getString(R.string.notification_added_to_db),
                                        Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.error_require_all_fields,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.general_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
