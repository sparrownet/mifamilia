package com.davidi.mifamilia.api;

import com.davidi.mifamilia.BaseFamily;
import com.davidi.mifamilia.db.Person;
import com.davidi.mifamilia.db.Relationship;

import java.util.List;

/**
 * Created by Dror on 29-Jun-16.
 */
public interface IFamilyApi {

    //TODO we can add here more relations, e.g. DIVORCED, IN_RELATIONSHIP
    enum RelationshipType {
        FATHER(0), MOTHER(1), CHILD(2), SIBLING(3), MARRIED(4);

        private final int value;

        RelationshipType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * Adds a person to the family tree
     * @param person
     * @return row id in database
     */
    long addPerson(Person person);

    /**
     * Deletes a person from the family tree
     * This cannot be reversed.
     * @param person the person to be deleted
     */
    void deletePerson(Person person);

    /**
     * update an existing person in the family tree
     * @param person
     */
    void updatePerson(Person person);

    /**
     * set a new relationship
     * @param relationship
     * @return row id in database
     */
    long createRelationship(Relationship relationship);

    /**
     * delete an existing relationship
     * @param relationship
     */
    void deleteRelationship(Relationship relationship);

    /**
     * update an exiting relationship
     * @param relationship
     */
    int updateRelationship(Relationship relationship);


    /**
     * get the closest relatives of a person
     *
     * @param person
     * @return
     */
    BaseFamily getNuclearFamily(Person person);

    /**
     * get the list fathers of a person
     * @param person
     * @return
     */
    List<Person> getFatherOf(Person person);

    /**
     * get the list of mothers of a person
     * @param person
     * @return
     */
    List<Person> getMotherOf(Person person);

    /**
     * get the list of children of a person
     * @param person
     * @return
     */
    List<Person> getChildrenOf(Person person);

    /**
     * get the list of siblings of a person
     * @param person
     * @return
     */
    List<Person> getSiblingsOf(Person person);

    /**
     * get the list of spouses of a person
     * @param person
     * @return
     */
    List<Person> getSpouseOf(Person person);

    /**
     * get all persons in database
     * @return
     */
    List<Person> getAllPersons();
}
