package com.davidi.mifamilia.api;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.widget.ListView;

import com.davidi.mifamilia.BaseFamily;
import com.davidi.mifamilia.db.DatabaseHelper;
import com.davidi.mifamilia.db.Person;
import com.davidi.mifamilia.db.Relationship;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dror on 30-Jun-16.
 */
public class FamilyApi implements IFamilyApi {

    public static final String GENDER_MALE = "Male";
    public static final String GENDER_FEMALE = "Female";

    private SQLiteDatabase mDb;

    public FamilyApi(Context context) {
        DatabaseHelper mDbHelper = new DatabaseHelper(context);
        mDb = mDbHelper.getWritableDatabase();
    }

    @Override
    public long addPerson(Person person) {

        long newRowId = -1;

        if (getPerson(person.getId()) == null) {

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(DatabaseHelper.KEY_PERSON_ID, person.getId());
            values.put(DatabaseHelper.KEY_PERSON_FIRST_NAME, person.getFirstName());
            values.put(DatabaseHelper.KEY_PERSON_LAST_NAME, person.getLastName());
            values.put(DatabaseHelper.KEY_PERSON_GENDER, person.getGender());
            values.put(DatabaseHelper.KEY_PERSON_BIRTH_YEAR, person.getBirthYear());

            newRowId = mDb.insert(
                    DatabaseHelper.TABLE_PERSONS,
                    null,
                    values);
        }

        return newRowId;
    }

    //TODO if we will support data about dead people, we should not remove persons and relationships!
    //TODO in this case we should keep a column saying "DEAD" or "ALIVE"
    @Override
    public void deletePerson(Person person) {

        // DELETE FROM PERSONS TABLE
        String selectionPersons = DatabaseHelper.KEY_PERSON_ID + " LIKE ? ";
        String[] selectionArgsPersons = { String.valueOf(person.getId()) };

        mDb.delete(DatabaseHelper.TABLE_PERSONS,
                    selectionPersons,
                    selectionArgsPersons);

        // DELETE FROM RELATIONS TABLE
        String selectionRelations = DatabaseHelper.KEY_ROOT_ID + " LIKE ? OR " + DatabaseHelper.KEY_RELATED_ID + " LIKE ? ";
        String[] selectionArgsRelations = { String.valueOf(person.getId()), String.valueOf(person.getId()) };

        mDb.delete(DatabaseHelper.TABLE_RELATIONS,
                selectionPersons,
                selectionArgsPersons);
    }

    @Override
    public void updatePerson(Person person) {

        // get the person new values
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.KEY_PERSON_ID, person.getFirstName());
        values.put(DatabaseHelper.KEY_PERSON_FIRST_NAME, person.getFirstName());
        values.put(DatabaseHelper.KEY_PERSON_LAST_NAME, person.getLastName());
        values.put(DatabaseHelper.KEY_PERSON_GENDER, person.getLastName());
        values.put(DatabaseHelper.KEY_PERSON_BIRTH_YEAR, person.getBirthYear());

        // Which row to update, based on the ID
        String selection = DatabaseHelper.KEY_PERSON_ID + " LIKE ? ";
        String[] selectionArgs = { String.valueOf(person.getId()) };

        mDb.update(
                DatabaseHelper.TABLE_PERSONS,
                values,
                selection,
                selectionArgs);

    }

    @Override
    public long createRelationship(Relationship relationship) {
        long newRowId = -1;

        //try updating, if the number of affected rows is zero, create new
        if (updateRelationship(relationship) == 0) {

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(DatabaseHelper.KEY_ROOT_ID, relationship.getRoot());
            values.put(DatabaseHelper.KEY_RELATED_ID, relationship.getRelated());
            values.put(DatabaseHelper.KEY_RELATION_TYPE, relationship.getType());

            // Insert the new row, returning the primary key value of the new row
            newRowId = mDb.insert(
                    DatabaseHelper.TABLE_RELATIONS,
                    null,
                    values);
        }


        return newRowId;
    }

    @Override
    public void deleteRelationship(Relationship relationship) {

        // Define 'where' part of query
        // Important assumption: in our DB there's only one relationship between root and related
        // (on that direction). otherwise, the IFamilyApi implementation should be changed.
        String selection =  DatabaseHelper.KEY_ROOT_ID + " LIKE ? AND " +
                              DatabaseHelper.KEY_RELATED_ID + " LIKE ? ";


        // Specify arguments in placeholder order
        String[] selectionArgs = { String.valueOf(relationship.getRoot()),
                                    String.valueOf(relationship.getRelated()) };

        // Issue SQL statement
        mDb.delete(DatabaseHelper.TABLE_RELATIONS,
                selection,
                selectionArgs);
    }

    @Override
    public int updateRelationship(Relationship relationship) {

        // get the person new values
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.KEY_ROOT_ID, relationship.getRoot());
        values.put(DatabaseHelper.KEY_RELATED_ID, relationship.getRelated());
        values.put(DatabaseHelper.KEY_RELATION_TYPE, relationship.getType());

        // Important assumption: in our DB there's only one relationship between root and related
        // (on that direction). otherwise, the IFamilyApi implementation should be changed.
        String selection =  DatabaseHelper.KEY_ROOT_ID + " LIKE ? AND " +
                DatabaseHelper.KEY_RELATED_ID + " LIKE ? ";

        String[] selectionArgs = { String.valueOf(relationship.getRoot()),
                String.valueOf(relationship.getRelated()) };

        return mDb.update(
                DatabaseHelper.TABLE_RELATIONS,
                values,
                selection,
                selectionArgs);
    }

    @Override
    public BaseFamily getNuclearFamily(Person person) {
        BaseFamily family = new BaseFamily(person);

        // get parents
        List<Person> parents = new ArrayList<>();
        parents.addAll(getFatherOf(person));
        parents.addAll(getMotherOf(person));
        family.setParents(parents);

        // get spouse
        List<Person> spouse = getSpouseOf(person);
        family.setSpouse(spouse);

        // get children
        List<Person> children = getChildrenOf(person);
        family.setChildren(children);

        // get siblings
        List<Person> siblings = getSiblingsOf(person);
        family.setSiblings(siblings);

        return family;
    }

    @Override
    public List<Person> getFatherOf(Person person) {
        return getRelativesOf(person, RelationshipType.FATHER);
    }

    @Override
    public List<Person> getMotherOf(Person person) {
        return getRelativesOf(person, RelationshipType.MOTHER);
    }

    @Override
    public List<Person> getChildrenOf(Person person) {
        return getRelativesOf(person, RelationshipType.CHILD);
    }

    @Override
    public List<Person> getSiblingsOf(Person person) {
        return getRelativesOf(person, RelationshipType.SIBLING);
    }

    @Override
    public List<Person> getSpouseOf(Person person) {
        return getRelativesOf(person, RelationshipType.MARRIED);
    }

    @Override
    public List<Person> getAllPersons() {
        ArrayList<Person> persons = new ArrayList<>();

        Cursor cursor = mDb.rawQuery("select * from " + DatabaseHelper.TABLE_PERSONS ,null);

        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String personId = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseHelper.KEY_PERSON_ID));
                String firstName = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseHelper.KEY_PERSON_FIRST_NAME));
                String lastName = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseHelper.KEY_PERSON_LAST_NAME));
                String gender = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseHelper.KEY_PERSON_GENDER));
                String birthDate = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseHelper.KEY_PERSON_BIRTH_YEAR));

                Person person = new Person(personId, firstName, lastName, gender, birthDate);
                persons.add(person);
            }
        }

        return persons;
    }

    /**
     * get Person object from the person's id
     * @param id
     * @return
     */
    public Person getPerson(String id) {

        Person person = null;

        String[] projection = {
                DatabaseHelper.KEY_PERSON_ID,
                DatabaseHelper.KEY_PERSON_FIRST_NAME,
                DatabaseHelper.KEY_PERSON_LAST_NAME,
                DatabaseHelper.KEY_PERSON_GENDER,
                DatabaseHelper.KEY_PERSON_BIRTH_YEAR};

        String selection = DatabaseHelper.KEY_PERSON_ID + " LIKE ? ";

        String[] selectionArgs = { id };

        Cursor cursor = mDb.query(
                DatabaseHelper.TABLE_PERSONS,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            try {
                String firstName = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseHelper.KEY_PERSON_FIRST_NAME));
                String lastName = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseHelper.KEY_PERSON_LAST_NAME));
                String gender = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseHelper.KEY_PERSON_GENDER));
                String birthDate = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseHelper.KEY_PERSON_BIRTH_YEAR));

                person = new Person(String.valueOf(id), firstName, lastName, gender, birthDate);

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }

        return person;
    }

    /**
     * get a list of all relatives of a person
     * @param person
     * @param type
     * @return
     */
    public List<Person> getRelativesOf(Person person, RelationshipType type) {

        ArrayList<Person> relatives = new ArrayList<>();

        String[] projection = {
                DatabaseHelper.KEY_RELATED_ID};

        String selection =  DatabaseHelper.KEY_ROOT_ID + " LIKE ? AND " +
                DatabaseHelper.KEY_RELATION_TYPE + " LIKE ? ";

        String[] selectionArgs = { String.valueOf(person.getId()),
                String.valueOf(type.getValue()) };

        Cursor cursor = mDb.query(
                DatabaseHelper.TABLE_RELATIONS,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if (cursor != null) {
            while (cursor.moveToNext()) {
                String personId = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseHelper.KEY_RELATED_ID));
                relatives.add(getPerson(personId));
            }
        }

        return relatives;
    }
}
