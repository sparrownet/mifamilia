package com.davidi.mifamilia;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Dror on 02-Jul-16.
 */
public class SplashActivity  extends AppCompatActivity {

    private final String SHARED_PREF_IS_DB_SET = "isDbSet";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //for demo purposes, we load demo data
        boolean isDbSet = getPreferences(MODE_PRIVATE).getBoolean(SHARED_PREF_IS_DB_SET, false);
        if (!isDbSet) {
            startFillingDemoData();
        } else {
            startMainActivity();
        }
    }

    private void startFillingDemoData(){
        new FillDemoData().execute();
    }

    private void startMainActivity(){
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private class FillDemoData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            final DatabaseFiller dbFiller = new DatabaseFiller(getApplicationContext());
            dbFiller.fillData();
            getPreferences(MODE_PRIVATE).edit().putBoolean(SHARED_PREF_IS_DB_SET, true).apply();
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            super.onPostExecute(v);
            startMainActivity();
        }
    }
}
