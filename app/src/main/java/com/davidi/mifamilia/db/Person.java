package com.davidi.mifamilia.db;

/**
 * Created by Dror on 29-Jun-16.
 */
public class Person {

    private String id;
    private String firstName;
    private String lastName;
    private String gender;
    private String birthYear;

    public Person(String id, String firstName, String lastName, String gender, String birthYear) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthYear = birthYear;
    }

    public String getId() {
        return id;
    }

    //TODO we can add setters if we want to support changing person name outside CTOR
    public String getBirthYear() {
        return birthYear;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return getFirstName() + " " + getLastName();
    }
}
