package com.davidi.mifamilia.db;

/**
 * Created by Dror on 29-Jun-16.
 */
public class Relationship {

    // the root person that we are asking about
    private String root;

    // the person who is related to the root
    private String related;

    // relation type
    private int type;

    public Relationship(String root, String related, int type) {
        this.root = root;
        this.related = related;
        this.type = type;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getRelated() {
        return related;
    }

    public void setRelated(String related) {
        this.related = related;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
