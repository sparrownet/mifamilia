package com.davidi.mifamilia.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Dror on 30-Jun-16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "familyTreeDb";

    // Persons table name
    public static final String TABLE_PERSONS = "table_persons";

    // Persons Table Columns names
    public static final String KEY_PERSON_ID = "id_number";
    public static final String KEY_PERSON_FIRST_NAME = "first_name";
    public static final String KEY_PERSON_LAST_NAME = "last_name";
    public static final String KEY_PERSON_GENDER = "gender";
    public static final String KEY_PERSON_BIRTH_YEAR = "birth_year";


    // Relations table name
    public static final String TABLE_RELATIONS = "table_relations";

    // Relations Table Columns names
    public static final String KEY_RELATION_ID = "relation_id";
    public static final String KEY_ROOT_ID = "root_id";
    public static final String KEY_RELATED_ID = "related_id";
    public static final String KEY_RELATION_TYPE = "relation_type";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PERSONS_TABLE = "CREATE TABLE " + TABLE_PERSONS +
                "(" + KEY_PERSON_ID + " TEXT PRIMARY KEY,"
                    + KEY_PERSON_FIRST_NAME + " TEXT,"
                    + KEY_PERSON_LAST_NAME + " TEXT,"
                    + KEY_PERSON_GENDER + " TEXT,"
                    + KEY_PERSON_BIRTH_YEAR + " INTEGER" + ")";
        db.execSQL(CREATE_PERSONS_TABLE);


        String CREATE_RELATIONS_TABLE = "CREATE TABLE " + TABLE_RELATIONS +
                "(" + KEY_RELATION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + KEY_ROOT_ID + " TEXT,"
                    + KEY_RELATED_ID + " TEXT,"
                    + KEY_RELATION_TYPE + " INTEGER" + ")";
        db.execSQL(CREATE_RELATIONS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PERSONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RELATIONS);

        // Create tables again
        onCreate(db);
    }
}
