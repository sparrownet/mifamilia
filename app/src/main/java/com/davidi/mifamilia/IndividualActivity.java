package com.davidi.mifamilia;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.davidi.mifamilia.db.Person;

import java.util.List;

/**
 * Created by Dror on 02-Jul-16.
 */
public class IndividualActivity extends AppCompatActivity {

    public static final String EXTRA_ID = "id";

    MiFamiliaApi mApi;
    RecyclerViewMinimizedAdapter mAdapterParents;
    RecyclerView mRecyclerViewParents;
    RecyclerViewMinimizedAdapter mAdapterSpouses;
    RecyclerView mRecyclerViewSpouses;
    RecyclerViewMinimizedAdapter mAdapterChildren;
    RecyclerView mRecyclerViewChildren;
    RecyclerViewMinimizedAdapter mAdapterSiblings;
    RecyclerView mRecyclerViewSiblings;

    BaseFamily mBaseFamily;

    View mThePersonView;
    View mSpouseView;
    View mChildrenView;
    View mParentsView;
    View mSiblingsView;
    Person mThePerson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        LinearLayoutManager llm;

        mThePersonView = findViewById(R.id.the_person_view);
        mSpouseView = findViewById(R.id.spouse_view);
        mParentsView = findViewById(R.id.parents_view);
        mChildrenView = findViewById(R.id.children_view);
        mSiblingsView = findViewById(R.id.siblings_view);

        mRecyclerViewParents = (RecyclerView)findViewById(R.id.recycler_view_parents);
        llm = new LinearLayoutManager(this);
        mRecyclerViewParents.setLayoutManager(llm);

        mRecyclerViewSpouses = (RecyclerView)findViewById(R.id.recycler_view_spouses);
        llm = new LinearLayoutManager(this);
        mRecyclerViewSpouses.setLayoutManager(llm);

        mRecyclerViewChildren = (RecyclerView)findViewById(R.id.recycler_view_children);
        llm = new LinearLayoutManager(this);
        mRecyclerViewChildren.setLayoutManager(llm);

        mRecyclerViewSiblings = (RecyclerView)findViewById(R.id.recycler_view_siblings);
        llm = new LinearLayoutManager(this);
        mRecyclerViewSiblings.setLayoutManager(llm);

        mApi = new MiFamiliaApi(getApplicationContext());

        Intent intent = getIntent();
        String id = intent.getStringExtra(EXTRA_ID);

        // get the person details on screen
        new GetPersonDetails().execute(id);

        // get family details
        new GetFamilyDetails().execute(id);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.general_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_home:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addListeners() {
        mRecyclerViewParents.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        String id = mBaseFamily.getParents().get(position).getId();
                        Intent intent = new Intent(IndividualActivity.this, IndividualActivity.class);
                        intent.putExtra(EXTRA_ID, id);
                        startActivity(intent);
                    }
                })
        );

        mRecyclerViewSpouses.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        String id = mBaseFamily.getSpouse().get(position).getId();
                        Intent intent = new Intent(IndividualActivity.this, IndividualActivity.class);
                        intent.putExtra(EXTRA_ID, id);
                        startActivity(intent);
                    }
                })
        );

        mRecyclerViewChildren.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        String id = mBaseFamily.getChildren().get(position).getId();
                        Intent intent = new Intent(IndividualActivity.this, IndividualActivity.class);
                        intent.putExtra(EXTRA_ID, id);
                        startActivity(intent);
                    }
                })
        );

        mRecyclerViewSiblings.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        String id = mBaseFamily.getSiblings().get(position).getId();
                        Intent intent = new Intent(IndividualActivity.this, IndividualActivity.class);
                        intent.putExtra(EXTRA_ID, id);
                        startActivity(intent);
                    }
                })
        );
    }

    private class GetPersonDetails extends AsyncTask<String, Person, Person> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Person doInBackground(String... ids) {
            Person thePerson = mApi.getPerson(ids[0]);
            mThePerson = thePerson;
            return thePerson;
        }

        @Override
        protected void onPostExecute(Person person) {
            fillData(mThePersonView, person);
        }
    }

    private class GetFamilyDetails extends AsyncTask<String, Person, BaseFamily> {

        @Override
        protected BaseFamily doInBackground(String... ids) {
            Person thePerson = mApi.getPerson(ids[0]);
            mBaseFamily = mApi.getNuclearFamily(thePerson);

            return mBaseFamily;
        }

        @Override
        protected void onPostExecute(BaseFamily baseFamily) {

            // fill parents
            List<Person> parents = baseFamily.getParents();
            mAdapterParents = new RecyclerViewMinimizedAdapter(parents);
            mRecyclerViewParents.setAdapter(mAdapterParents);

            // fill spouse
            List<Person> spouses = baseFamily.getSpouse();
            mAdapterSpouses = new RecyclerViewMinimizedAdapter(spouses);
            mRecyclerViewSpouses.setAdapter(mAdapterSpouses);

            // fill children
            List<Person> children = baseFamily.getChildren();
            mAdapterChildren = new RecyclerViewMinimizedAdapter(children);
            mRecyclerViewChildren.setAdapter(mAdapterChildren);

            // fill siblings
            List<Person> siblings = baseFamily.getSiblings();
            mAdapterSiblings = new RecyclerViewMinimizedAdapter(siblings);
            mRecyclerViewSiblings.setAdapter(mAdapterSiblings);

            addListeners();

            if (baseFamily.getParents().size() > 0) {
                mParentsView.setVisibility(View.VISIBLE);
            } else {
                mParentsView.setVisibility(View.GONE);
            }

            if (baseFamily.getSpouse().size() > 0) {
                mSpouseView.setVisibility(View.VISIBLE);
            } else {
                mSpouseView.setVisibility(View.GONE);
            }

            if (baseFamily.getChildren().size() > 0) {
                mChildrenView.setVisibility(View.VISIBLE);
            } else {
                mChildrenView.setVisibility(View.GONE);
            }

            if (baseFamily.getSiblings().size() > 0) {
                mSiblingsView.setVisibility(View.VISIBLE);
            } else {
                mSiblingsView.setVisibility(View.GONE);
            }
        }
    }

    private void fillData(View personView, Person person) {
        TextView personName = (TextView) personView.findViewById(R.id.person_name);
        TextView personId = (TextView) personView.findViewById(R.id.person_id);
        TextView personGender = (TextView) personView.findViewById(R.id.person_gender);
        TextView personBirthYear = (TextView) personView.findViewById(R.id.person_birth_year);

        String name = person.getFirstName() + " " + person.getLastName();
        personName.setText(name);

        String id = "ID: " + person.getId();
        personId.setText(id);

        String gender = "Gender: " + person.getGender();
        personGender.setText(gender);

        String birthYear = "Birth Year: " + person.getBirthYear();
        personBirthYear.setText(birthYear);

        personView.setVisibility(View.VISIBLE);
    }

}
