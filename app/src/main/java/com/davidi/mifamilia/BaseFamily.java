package com.davidi.mifamilia;

import com.davidi.mifamilia.db.Person;

import java.util.List;

/**
 * Created by Dror on 30-Jun-16.
 *
 * This class represents a base nuclear family of a root person
 */
public class BaseFamily {

    private Person root;
    private List<Person> parents;
    private List<Person> siblings;
    private List<Person> children;
    private List<Person> spouse;

    // CTOR of smallest root family is the root itself
    public BaseFamily(Person root) {
        this.root = root;
    }

    public Person getRoot() {
        return root;
    }

    public void setRoot(Person root) {
        this.root = root;
    }

    public List<Person> getParents() {
        return parents;
    }

    public void setParents(List<Person> parents) {
        this.parents = parents;
    }

    public List<Person> getSiblings() {
        return siblings;
    }

    public void setSiblings(List<Person> siblings) {
        this.siblings = siblings;
    }

    public List<Person> getChildren() {
        return children;
    }

    public void setChildren(List<Person> children) {
        this.children = children;
    }

    public List<Person> getSpouse() {
        return spouse;
    }

    public void setSpouse(List<Person> spouse) {
        this.spouse = spouse;
    }
}
